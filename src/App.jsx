import { Component } from "react";
import Button from "./components/ui/Button/Button";
import Modal from "./components/ui/Modal/Modal";
import s from "./App.module.scss";
import Header from "./components/ui/Header/Header";
import Main from "./components/ui/Main/Main";
import Footer from "./components/ui/Footer/Footer";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFirstModalOpened: false,
      isSecondModalOpened: false,
      cards: [],
      cartItemsAmount: 0,
      addedToShoppingCart: [],
      shoppingCard: [],
      addedToFavorites:[],
      favoritesAmount:0
    };
  }

  componentDidMount() {
    const shoppingCard = localStorage.getItem("ShoppingCard");
    const favorites = localStorage.getItem("Favorites")
    if (shoppingCard) {
      this.setState({
        shoppingCard: JSON.parse(shoppingCard),
        cartItemsAmount: JSON.parse(shoppingCard).length,
      });
    }
    if (favorites){
      this.setState({
        addToFavorites: JSON.parse(favorites),
        favoritesAmount: JSON.parse((favorites)).length
      })
    }
    fetch("/Cards.json")
      .then((res) => res.json())
      .then((result) => {
        this.setState({
          cards: result,
        });
      });
  }

  handleOpeningFirstModal = (url, name, price, artikle) => {
    this.setState({
      isFirstModalOpened: true,
      isSecondModalOpened: false,
    });
    this.setState({
      addedToShoppingCart: [
        ...this.state.addedToShoppingCart,
        { url, name, price, artikle },
      ],
    });
  };

  handleOpeningSecondModal = () => {
    this.setState({
      isFirstModalOpened: false,
      isSecondModalOpened: true,
    });
  };

  handleCloseModal = () => {
    this.setState({
      isFirstModalOpened: false,
      isSecondModalOpened: false,
      addedToShoppingCart: [],
    });
  };
  handleCartAmount = () => {
    const shoppingCart = [...this.state.addedToShoppingCart];
    this.setState(
      (prevState) => ({
        cartItemsAmount: prevState.cartItemsAmount + 1,
        shoppingCard: [...prevState.shoppingCard, ...shoppingCart],
      }),
      () => {
        localStorage.setItem(
          "ShoppingCard",
          JSON.stringify(this.state.shoppingCard)
        );
        this.handleCloseModal();
      }
    );
  };

  handleAddToFavorites = (url, name, price, artikle) => {
    this.setState(prevState => {
      const isItemExist = prevState.addedToFavorites.some(item => item.artikle === artikle);
      if (!isItemExist) {
        return {
          addedToFavorites: [...prevState.addedToFavorites, { url, name, price, artikle }],
          favoritesAmount: prevState.favoritesAmount +1
        };
      }
      return null;
    }, () => {
      localStorage.setItem(
        "Favorites",
        JSON.stringify(this.state.addedToFavorites)
      );
    });
  };

 handleDeleteFromFavorites = (artikle) => {
  this.setState(
    (prevState) => {
      const updatedFavorites = prevState.addedToFavorites.filter(
        (item) => item.artikle !== artikle
      );
      return {
        addedToFavorites: updatedFavorites,
        favoritesAmount: prevState.favoritesAmount -1
      };
      
    },
    () => {
      localStorage.setItem(
        "Favorites",
        JSON.stringify(this.state.addedToFavorites)
      );
    }
  );
};

  render() {
    return (
      <div className={s.App}>
        <Header favoritesAmount={this.state.favoritesAmount} cartAmount={this.state.cartItemsAmount} />
        <Main
          addToFavorites={this.handleAddToFavorites}
          addToCart={this.handleOpeningFirstModal}
          cards={this.state.cards}
          deleteFavorite={this.handleDeleteFromFavorites}
        />

        <Modal
          onClick={this.handleCloseModal}
          closeButton={true}
          isOpened={this.state.isFirstModalOpened}
          actions={
            <>
              <button
                onClick={this.handleCartAmount}
                className={s.ConfirmAddBtn}
              >
                Ok
              </button>
              <button
                className={s.ConfirmAddBtn}
                onClick={this.handleCloseModal}
              >
                Cancel
              </button>
            </>
          }
          header="FLASH ENERGY"
          text={"ADD ITEM TO SHOPPING CART?"}
          backgroundColor={"rgba(31, 29, 29, 1)"}
          headerBackgroundColor={"rgba(20, 18, 18, 1)"}
        ></Modal>
        <Footer/>
        {/* <Modal
          closeButton={true}
          onClick={this.handleCloseModal}
          isOpened={this.state.isSecondModalOpened}
          header="Do you want to delete this file?"
          text={
            "Once you delete this file, it won't be possible to undo this action.Are you sure you want to delete it?"
          }
          actions={
            <>
              <button className={s.confirmModalButton}>Ok</button>
              <button className={s.confirmModalButton} onClick={this.handleCloseModal}>Відміна</button>
            </>
          }
        ></Modal> */}
      </div>
    );
  }
}

export default App;
