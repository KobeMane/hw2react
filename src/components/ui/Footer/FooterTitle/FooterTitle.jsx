import { Component } from "react";
import s from './FooterTitle.module.scss'
import footerTitle from '../../../../images/FooterTitle.png'
class FooterTitle extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className={s.FooterTitle}>
                <img className={s.footerImage} src={footerTitle} alt="drifting" />
                <div className={s.titleDescriptioon}>DRIFT MASTERS EUROPEAN CHAMPIONSHIP</div>
                <button className={s.footerButton}>EVENT DETAILS</button>
            </div>
        )
    }
}

export default FooterTitle;