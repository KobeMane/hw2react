import { Component } from "react";
import s from './Footer.module.scss'
import FooterTitle from "./FooterTitle/FooterTitle";

class Footer extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <footer className={s.footer}>
                <FooterTitle/>
            </footer>
        )
    }
}

export default Footer;