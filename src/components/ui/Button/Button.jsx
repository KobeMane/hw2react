import { Component } from "react";
import s from './Button.module.scss'

class Button extends Component {
  constructor(props) {
    super(props);
  
  }

  render() {
    const { addToCart } = this.props;
    return (
      <button className={s.mainButton} onClick={addToCart}>
       ADD TO CART
      </button>
    );
  }
}

export default Button;
