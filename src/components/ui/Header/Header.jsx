import { Component } from "react";
import HeaderNav from "./HeaderNav/HeaderNav";
import s from './Header.module.scss'
import titleImg from '../../../images/TitleImage.png'
import logoSec  from '../../../images/logoSec.png' 
import logoFirst  from '../../../images/logoFirst.png' 

class Header extends Component {
    constructor(props){
        super(props)
        this.state ={
            
        }
    }
    render() {
        const { cartAmount, addedCardsToCart, favoritesAmount } = this.props;
        return (
            <header className={s.header}>
                <HeaderNav favoritesAmount={favoritesAmount} addedCardsToCart={addedCardsToCart} cartAmount={cartAmount}/>
                <div className={s.title}>
                <img src={logoFirst} className={s.titleLogo}/>
                <img src={titleImg} alt="Flash Energy" className={s.titleImage} />
                <img src={logoSec} alt="eawd" className={s.titleLogo}/>
                </div>
            </header>
        )
    }
}

export default Header;