import { Component } from "react";
import shoppingCartLogo from "../../../../../images/CartIcon.png";
import s from "./ShoppingCart.module.scss";

class ShoppingCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShoppingCartOpened: false,
    };
  }

  handleOpenShoppingCart = () => {
    if (this.state.isShoppingCartOpened === false) {
      this.setState({
        isShoppingCartOpened: true,
      });
    } else {
      this.setState({
        isShoppingCartOpened: false,
      });
    }
  };
  



  render() {
    const { isShoppingCartOpened, addedCardsToCart } = this.state;
    return (
      <div className={s.shoppingCartWrapper}>
        <img
          onClick={this.handleOpenShoppingCart}
          src={shoppingCartLogo}
          alt="Shopping Cart"
          className={s.shoppingCartImage}
        />
        {isShoppingCartOpened && 
        <div className={s.shoppingCart}>
            shopping card
        </div>}
      </div>
    );
  }
}

export default ShoppingCart;
