import { Component } from "react";
import  logo  from '../../../../images/logo.png'
import Login from "../Login/Login";
import Favorites from "./Favorites/Favorites";
import s from './HeaderNav.module.scss'
import ShoppingCart from "./ShoppingCart/ShoppingCart";

class HeaderNav extends Component {
    constructor(props){
        super(props)
    }
    render(){
        const {cartAmount, addedCardsToCart, favoritesAmount} = this.props;
        return(
        <nav className={s.headerNav}>
            <ul className={s.navList}>
                <li className={s.navItem}>PRODUCTION</li>
                <li className={s.navItem}>TV</li>
                <li className={s.navItem}>ABOUT US</li>
                <img className={s.logo} src={logo} alt="Flash Energy" />
                <li className={s.navItem}>NEW</li>
            </ul>
            <div className={s.userNav}>
                <Login/>
                <Favorites/>
                {favoritesAmount}
                <ShoppingCart addedCardsToCart={addedCardsToCart}/>
                <div className={s.cartAmount}>{cartAmount}</div>
                </div>
        </nav>
        )
    }
}

export default HeaderNav;