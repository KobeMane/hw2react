import { Component } from "react";
import shoppingCartLogo from "../../../../../images/favoritespng.png";
import s from "./Favorites.module.scss";

class Favorites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShoppingCartOpened: false,
    };
  }

  handleOpenShoppingCart = () => {
    if (this.state.isShoppingCartOpened === false) {
      this.setState({
        isShoppingCartOpened: true,
      });
    } else {
      this.setState({
        isShoppingCartOpened: false,
      });
    }
  };
  
  render() {
    const { isShoppingCartOpened } = this.state;
    return (
      <div className={s.favoritesWrapper}>
        <img
          onClick={this.handleOpenShoppingCart}
          src={shoppingCartLogo}
          alt="Shopping Cart"
          className={s.FavoritesImage}
        /> 
        {isShoppingCartOpened && 
        <div className={s.favorites}>
            favorites
        </div>}
      </div>
    );
  }
}

export default Favorites;
