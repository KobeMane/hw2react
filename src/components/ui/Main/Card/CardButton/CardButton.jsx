import { Component } from "react";
import s from './CardButton.module.scss'

class CardButton extends Component {
  constructor(props) {
    super(props);
  
  }

  render() {
    const { handleAddedItem } = this.props;
    return (
      <button className={s.mainButton} onClick={handleAddedItem}>
       ADD TO CART
      </button>
    );
  }
}

export default CardButton;
