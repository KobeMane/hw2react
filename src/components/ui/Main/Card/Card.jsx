import React, { Component } from "react";
import s from './Card.module.scss';
import CardButton from "./CardButton/CardButton";

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAddedToFavorite: false
        };
    }

    componentDidMount() {
        const { artikle } = this.props;
        const addedToFavorites = localStorage.getItem('addedToFavorites');

        if (addedToFavorites) {
            const favorites = JSON.parse(addedToFavorites);
            const isAddedToFavorite = favorites.some(item => item.artikle === artikle);
            if (isAddedToFavorite) {
                this.setState({ isAddedToFavorite: true });
            }
        }
    }

    handleAddToFavIcon = () => {
        this.setState(prevState => ({
            isAddedToFavorite: !prevState.isAddedToFavorite
        }), () => {
            const { artikle } = this.props;
            const { isAddedToFavorite } = this.state;

            const addedToFavorites = localStorage.getItem('addedToFavorites');
            let favorites = [];

            if (addedToFavorites) {
                favorites = JSON.parse(addedToFavorites);
            }

            if (isAddedToFavorite) {
                favorites.push({ artikle });
            } else {
                favorites = favorites.filter(item => item.artikle !== artikle);
            }

            localStorage.setItem('addedToFavorites', JSON.stringify(favorites));
        });
    }

    handleAddToFavorite = () => {
        const { addToFavorites, image, name, price, artikle } = this.props;
        addToFavorites(image, name, price, artikle);
    };

    handleAddedItem = () => {
        const { image, name, price, addToCart, artikle } = this.props;
        addToCart(image, name, price, artikle);
    };

    handleDEleteItem = () =>{
        const {name,image,price, artikle, deleteFavorite} = this.props;
        deleteFavorite(artikle);
    }

    render() {
        const { isAddedToFavorite } = this.state;
        const { name, image, price, addToCart } = this.props;
        return (
            <div className={s.card}>
                <span onClick={this.handleAddToFavIcon} className={s.FavoriteIcon}>
                    {isAddedToFavorite ? <div onClick={this.handleDEleteItem}>&#128154;</div> : <div onClick={this.handleAddToFavorite}>&#129293;</div>}
                </span>
            
                <img className={s.cardImage} src={image} alt="Energy Drink" />
                <div className={s.cardInfo}>
                    <div className={s.CardTitle}>{name}</div>
                    <div className={s.cardMain}>
                        <div className={s.price}>{price} $</div>
                        <CardButton handleAddedItem={this.handleAddedItem} addToCart={addToCart} className={s.addCardButton} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Card;
