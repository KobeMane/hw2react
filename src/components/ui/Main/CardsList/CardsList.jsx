import { Component } from "react";
import Card from "../Card/Card";
import s from './CardsList.module.scss'

class CardsList extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const {cards, addToCart, addToFavorites, deleteFavorite} = this.props;
        const renderedCards = cards.map((card) => {
            return <Card key={card.artikle} name={card.name} image={card.url} price={card.price} artikle={card.artikle} addToCart={addToCart} addToFavorites={addToFavorites} deleteFavorite={deleteFavorite}/>
        })
        return (
            <div className={s.cardsList}>{renderedCards}</div>
        )
    }
}

export default CardsList;