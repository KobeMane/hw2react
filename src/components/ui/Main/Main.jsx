import { Component } from "react";
import CardsList from "./CardsList/CardsList";
import s from './Main.module.scss'
class Main extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { cards, addToCart ,addToFavorites, deleteFavorite } = this.props
        return (
            <main className={s.main}>
                <p className={s.mainTitle}>DO YOU WANT TO FEEL ENEGRGIZET AND STRONG?</p>
                <CardsList addToFavorites={addToFavorites} addToCart={addToCart} cards={cards} deleteFavorite={deleteFavorite}/>
            </main>
        )
    }
}

export default Main;