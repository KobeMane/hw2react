import { Component } from "react";
import Card from "../Main/Card/Card";
import s from "./Modal.module.scss";

class Modal extends Component {
  constructor(props) {
    super(props);
  }

  stopPropagation = (e) => {
    e.stopPropagation();
  }

  render() {
    const { header, closeButton, text, onClick, isOpened, actions, backgroundColor, headerBackgroundColor } = this.props;
    return isOpened ? (
      <div className={s.modal} onClick={onClick} >
        <div className={s.modalContent} onClick={this.stopPropagation} style={{backgroundColor}}>
          <div style={{backgroundColor: headerBackgroundColor}} className={s.modalHeader}>
            {header}
            {closeButton ? <p className={s.closeButton} onClick={onClick}>Х</p> : false}
          </div>
          <p className={s.modalText}>{text}</p>
          {actions && <div className={s.modalActions}>{actions}</div>}
        </div>
      </div>
    ) : (
      false
    );
  }
}

export default Modal;
